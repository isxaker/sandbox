﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Restore.Converters
{
    public class NodeStatusToIconConverter : IValueConverter
    {
        private readonly SortedList<string, Uri> _statusIcons = new SortedList<string, Uri>
        {
            {ModelStatus.Error.ToString(), StatusIcons.ErrorIcon},
            {ModelStatus.Warning.ToString(), StatusIcons.WarningIcon},
            {ModelStatus.Success.ToString(), StatusIcons.SuccessIcon},
        };

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string type = value as string;
            if (type == null)
                return null;

            Uri uri = type.StartsWith(TreeNodeTypes.List) ? NodeIcons.ListIcon : NodeIcons.DefaultIcon;
            return NodeIconGetter.GetIcon(_statusIcons, uri, type);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }

    public static class StatusIcons
    {
        private static readonly Uri BaseUri = new Uri("pack://application:,,,/Restore;component/Images/");
        public static readonly Uri ErrorIcon = new Uri(BaseUri, "error.png");
        public static readonly Uri WarningIcon = new Uri(BaseUri, "warning.png");
        public static readonly Uri SuccessIcon = new Uri(BaseUri, "success.png");
    }
}
