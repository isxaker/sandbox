using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace Restore.Converters
{
    public class NodeTypeAndStatusToIconMultiConverter : IMultiValueConverter
    {

        private readonly SortedList<string, Uri> _statusIcons = new SortedList<string, Uri>
        {
            {ModelStatus.Error.ToString(), StatusIcons.ErrorIcon},
            {ModelStatus.Warning.ToString(), StatusIcons.WarningIcon},
            {ModelStatus.Success.ToString(), StatusIcons.SuccessIcon},
        };

        public static class ItemWithStatusIcons
        {
            private static readonly Uri BaseUri = new Uri("pack://application:,,,/Restore;component/Images/");

            public static readonly Uri ListError = new Uri(BaseUri, "list_error.png");
            public static readonly Uri ListWarning = new Uri(BaseUri, "list_warning.png");
            public static readonly Uri ListSuccess = new Uri(BaseUri, "Lists/ITGEN.png");

            public static readonly Uri SiteError = new Uri(BaseUri, "site_error.png");
            public static readonly Uri SiteWarning = new Uri(BaseUri, "site_warning.png");
            public static readonly Uri SiteSuccess = new Uri(BaseUri, "Web.ico");


        }

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var type = values[0] as string;
            if (type == null)
                return null;

            var status = values[1] as string;
            if (status == null)
                return null;

            Uri imageUri;
            switch (status)
            {
                case "warning":
                    {
                        switch (type)
                        {
                            case "list":
                                imageUri = ItemWithStatusIcons.ListWarning;
                                break;
                            case "web":
                                imageUri = ItemWithStatusIcons.SiteWarning;
                                break;
                            default:
                                throw new NotImplementedException();
                        }
                        break;
                    }
                case "error":
                    switch (type)
                    {
                        case "list":
                            imageUri = ItemWithStatusIcons.ListError;
                            break;
                        case "web":
                            imageUri = ItemWithStatusIcons.SiteError;
                            break;
                        default:
                            throw new NotImplementedException();
                    }
                    break;
                case "success":
                    switch (type)
                    {
                        case "list":
                            imageUri = ItemWithStatusIcons.ListSuccess;
                            break;
                        case "web":
                            imageUri = ItemWithStatusIcons.SiteSuccess;
                            break;
                        default:
                            throw new NotImplementedException();
                    }
                    break;
                default:
                    throw new NotImplementedException($"Status: {status} is not implemented yet");
            }

            return new BitmapImage(imageUri);

        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}