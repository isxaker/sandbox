﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace Restore.Converters
{
    public class NodeTypeToIconConverter : IValueConverter
    {
        private readonly SortedList<string, Uri> _nodeIcons = new SortedList<string, Uri>
        {
            {TreeNodeTypes.Databases, NodeIcons.DatabasesIcon},
            {TreeNodeTypes.Database, NodeIcons.DatabaseIcon},
            {TreeNodeTypes.Home, NodeIcons.HomeIcon},
            {TreeNodeTypes.Webs, NodeIcons.WebsIcon},
            {TreeNodeTypes.Web, NodeIcons.WebIcon},
            {TreeNodeTypes.Lists, NodeIcons.ListsIcon},

            {TreeNodeTypes.Announcements, NodeIcons.AnnouncementsIcon},
            {TreeNodeTypes.Contacts, NodeIcons.ContactsIcon},
            {TreeNodeTypes.Discussion, NodeIcons.DiscussionIcon},

            // Libraries
            {TreeNodeTypes.AssetLibrary, NodeIcons.LibraryIcon},
            {TreeNodeTypes.DocumentLibrary, NodeIcons.LibraryIcon},
            {TreeNodeTypes.NoCodeWorkflows, NodeIcons.LibraryIcon},
            {TreeNodeTypes.NoCodePublicWorkflows, NodeIcons.LibraryIcon},
            {TreeNodeTypes.PagesLibrary, NodeIcons.LibraryIcon},
            {TreeNodeTypes.SlideLibrary, NodeIcons.LibraryIcon},
            {TreeNodeTypes.WorkspacePages, NodeIcons.LibraryIcon},

            {TreeNodeTypes.Calendar, NodeIcons.EventsIcon},
            {TreeNodeTypes.Links, NodeIcons.LinksIcon},
            {TreeNodeTypes.Pictures, NodeIcons.PicturesIcon},
            {TreeNodeTypes.Survey, NodeIcons.SurveysIcon},

            // Tasks
            {TreeNodeTypes.AdminTasks, NodeIcons.TasksIcon},
            {TreeNodeTypes.ProjectTasks, NodeIcons.TasksIcon},
            {TreeNodeTypes.Tasks, NodeIcons.TasksIcon},

            {TreeNodeTypes.FormLibrary, NodeIcons.FormIcon},
        };

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string type = value as string;
            if (type == null)
                return null;

            Uri uri = type.StartsWith(TreeNodeTypes.List) ? NodeIcons.ListIcon : NodeIcons.DefaultIcon;
            return NodeIconGetter.GetIcon(_nodeIcons, uri, type);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }

    public static class TreeNodeTypes
    {
        public const string Databases = "Databases";
        public const string Database = "Database";
        public const string Home = "Home";
        public const string Webs = "Webs";
        public const string Web = "Web";
        public const string Lists = "Lists";
        public const string List = "List.";

        public const string Announcements = List + "104";
        public const string Contacts = List + "105";
        public const string Discussion = List + "108";

        // Libraries
        public const string AssetLibrary = List + "851";
        public const string DocumentLibrary = List + "101";
        public const string NoCodeWorkflows = List + "117";
        public const string NoCodePublicWorkflows = List + "122";
        public const string PagesLibrary = List + "119";
        public const string SlideLibrary = List + "2100";
        public const string WorkspacePages = List + "212";

        public const string Calendar = List + "106";
        public const string Links = List + "103";
        public const string Pictures = List + "109";
        public const string Survey = List + "102";

        // Tasks
        public const string AdminTasks = List + "1200";
        public const string ProjectTasks = List + "150";
        public const string Tasks = List + "107";

        public const string FormLibrary = List + "115";
    }

    public static class NodeIcons
    {
        private static readonly Uri BaseUri = new Uri("pack://application:,,,/Restore;component/Images/");
        public static readonly Uri DefaultIcon = new Uri(BaseUri, "Default.ico");
        public static readonly Uri DatabasesIcon = new Uri(BaseUri, "Dbs.ico");
        public static readonly Uri DatabaseIcon = new Uri(BaseUri, "Db.ico");
        public static readonly Uri HomeIcon = new Uri(BaseUri, "Web.ico");
        public static readonly Uri WebsIcon = new Uri(BaseUri, "Webs.ico");
        public static readonly Uri WebIcon = new Uri(BaseUri, "Web.ico");
        public static readonly Uri ListsIcon = new Uri(BaseUri, "Lists.ico");
        private static readonly Uri ListsBaseUri = new Uri(BaseUri, "Lists/");
        public static readonly Uri ListIcon = new Uri(ListsBaseUri, "ITGEN.PNG");

        public static readonly Uri AnnouncementsIcon = new Uri(ListsBaseUri, "ITANN.PNG");
        public static readonly Uri ContactsIcon = new Uri(ListsBaseUri, "ITCONTCT.PNG");
        public static readonly Uri DiscussionIcon = new Uri(ListsBaseUri, "ITDISC.PNG");
        public static readonly Uri LibraryIcon = new Uri(ListsBaseUri, "ITDL.PNG");
        public static readonly Uri EventsIcon = new Uri(ListsBaseUri, "ITEVENT.PNG");
        public static readonly Uri LinksIcon = new Uri(ListsBaseUri, "ITLINK.PNG");
        public static readonly Uri PicturesIcon = new Uri(ListsBaseUri, "ITIL.PNG");
        public static readonly Uri SurveysIcon = new Uri(ListsBaseUri, "ITSURVEY.PNG");
        public static readonly Uri TasksIcon = new Uri(ListsBaseUri, "ITTASK.PNG");
        public static readonly Uri FormIcon = new Uri(ListsBaseUri, "ITDL.PNG");
    }

    public static class NodeIconGetter
    {
        private static readonly SortedList<string, BitmapImage> IconsCache = new SortedList<string, BitmapImage>();

        public static BitmapImage GetIcon(SortedList<string, Uri> nodeIcons, Uri uri, string type)
        {
            if (nodeIcons.ContainsKey(type))
                uri = nodeIcons[type];

            if (!IconsCache.ContainsKey(uri.AbsoluteUri))
                IconsCache.Add(uri.AbsoluteUri, new BitmapImage(uri));

            return IconsCache[uri.AbsoluteUri];
        }
    }
}
