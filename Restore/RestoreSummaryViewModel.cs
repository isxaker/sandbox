﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Practices.Prism;
using Microsoft.Practices.Prism.Commands;
using Restore.Annotations;
using Veeam.SharePoint.Restore;

namespace Restore
{
    public class RestoreSummaryViewModel
    {
        public string Header { get; set; }
        public string RestoreSummaryMessage { get; private set; }
        public string RestoreGlobalStatus { get; set; }

        private readonly bool _haveWarnings;
        private int _skippedItemsCount;
        private int _skippedItemsByNoChangesCount;
        private int _failedItemsCount;
        private int _failedRestrictions;
        private readonly bool _hasRestored;
        private readonly bool _hasSkipped;
        private readonly bool _hasSkippedByError;
        private readonly bool _hasSkippedByChanges;
        private readonly bool _hasFailed;
        private readonly bool _hasRestrictionRestoreFailed;

        public ImageSource StatusImage { get; }

        public List<Model> RestoredItems { get; set; }

        private readonly int _restoredSuccessItemCount;
        public int RestoredSuccessItemsCounnt => _restoredSuccessItemCount;

        private readonly int _restoredWarningItemCount;
        public int RestoredWarningItemsCount => _restoredWarningItemCount;

        private readonly int _restoredErrorItemCount;

        public int RestoredErrorItemsCount => _restoredErrorItemCount;

        public ulong TotalItems => (ulong)RestoredItems.Count();

        public Action CloseAction { get; set; }
        public ICommand CancelCommand => new DelegateCommand(() => CloseAction());

        //public ICommand FilterSuccessCommand => new DelegateCommand<object>(i => GetItemSource(c => c.Status == ModelStatus.Success.ToString().ToLower() && (bool)i));
        //public ICommand FilterErrorCommand => new DelegateCommand<object>(i => GetItemSource(c => c.Status == ModelStatus.Error.ToString().ToLower() && (bool)i));
        //public ICommand FilterWarningCommand => new DelegateCommand<object>(i => GetItemSource(c => c.Status == ModelStatus.Warning.ToString().ToLower() && (bool)i));

        public ObservableCollection<Model> ItemsSource { get; set; } = new ObservableCollection<Model>();

        private bool _isShowSuccess;
        private bool _isShowWarning;
        private bool _isShowError;

        public bool IsShowSuccess
        {
            get { return _isShowSuccess; }
            set
            {
                if (_isShowSuccess == value) return;
                _isShowSuccess = value;
                ResfreshItemSource();
            }

        }

        private void ResfreshItemSource()
        {
            ItemsSource.Clear();
            ItemsSource.AddRange(GetItemSource(c => c.Status == ModelStatus.Success.ToString().ToLower() && _isShowSuccess));
            ItemsSource.AddRange(GetItemSource(c => c.Status == ModelStatus.Error.ToString().ToLower() && _isShowError));
            ItemsSource.AddRange(GetItemSource(c => c.Status == ModelStatus.Warning.ToString().ToLower() && _isShowWarning));
        }

        public bool IsShowWarning
        {
            get { return _isShowWarning; }
            set
            {
                if (_isShowWarning == value) return;
                _isShowWarning = value;
                ResfreshItemSource();
            }
        }

        public bool IsShowError
        {
            get { return _isShowError; }
            set
            {
                if (_isShowError == value) return;
                _isShowError = value;
                ResfreshItemSource();
            }
        }

        private List<Model> GetItemSource(Func<Model, bool> predicate)
        {
            return this.RestoredItems.Where(predicate).ToList();
        }

        public RestoreSummaryViewModel()
        {
        }

        public RestoreSummaryViewModel(Action close)
        {
            //fake
            this.RestoredItems = RestoreSummaryViewModel.BuildData();

            this.CloseAction = close;

            _restoredSuccessItemCount = RestoredItems.Count(i => i.Status == ModelStatus.Success.ToString().ToLower());
            _restoredWarningItemCount = RestoredItems.Count(i => i.Status == ModelStatus.Warning.ToString().ToLower());
            _restoredErrorItemCount = RestoredItems.Count(i => i.Status == ModelStatus.Error.ToString().ToLower());


            _hasFailed = _failedItemsCount > 0;
            _hasSkippedByError = _restoredErrorItemCount > 0;
            _hasSkipped = _skippedItemsCount > 0;
            _hasSkippedByChanges = _skippedItemsByNoChangesCount > 0;
            _hasRestrictionRestoreFailed = _failedRestrictions > 0;

            IsShowSuccess = _hasRestored = _restoredSuccessItemCount > 0;
            IsShowError = _hasFailed || _hasSkippedByError;
            IsShowWarning = _haveWarnings = _restoredWarningItemCount > 0;

            RestoreSummaryMessage = ConstructRestoreSummaryMessage("Data has been restored.");

            if (RestoreGlobalStatus == "error")
            {
                StatusImage = new BitmapImage(new Uri("pack://application:,,,/Restore;component/Images/status_error.png"));
            }
            else if (RestoreGlobalStatus == "success")
            {
                StatusImage = new BitmapImage(new Uri("pack://application:,,,/Restore;component/Images/status_success.png"));
            }
            else if (RestoreGlobalStatus == "warning")
            {
                StatusImage = new BitmapImage(new Uri("pack://application:,,,/Restore;component/Images/status_warning.png"));
            }
        }

        private string ConstructRestoreSummaryMessage(string sourceMessage)
        {
            var message = new StringBuilder();
            string header = "Restore summary:";

            if (_hasRestored)
            {
                message.AppendLine($"{_restoredSuccessItemCount} items restored.");
            }

            if (_hasSkipped)
            {
                message.AppendLine($"{_skippedItemsCount} items skipped.");
            }

            if (_hasFailed)
            {
                message.AppendLine($"{_failedItemsCount} items failed.");
            }

            if (_haveWarnings)
            {
                message.AppendLine("Restore completed with warnings.");
            }

            if (_hasRestrictionRestoreFailed)
            {
                message.AppendLine($"Failed to set properties for {_failedRestrictions} fields.");
            }

            if (_haveWarnings || _hasRestrictionRestoreFailed)
            {
                message.AppendLine("See log file for details");
            }

            if (!_hasRestored && _hasFailed || _hasRestrictionRestoreFailed)
            {
                //error
                RestoreGlobalStatus = "error";
                return message.ToString();
            }
            if ((_hasRestored && _hasFailed) || _hasSkippedByError || _haveWarnings)
            {
                //warning
                RestoreGlobalStatus = "warning";
                return message.ToString();
            }
            if (_hasRestored || _hasSkippedByChanges)
            {
                RestoreGlobalStatus = "success";
                return message.ToString();
            }

            RestoreGlobalStatus = "success";
            return sourceMessage;
        }

        private static List<Model> BuildData()
        {
            Random r = new Random();
            int i = r.Next(2);

            if (i == 0)
            {
                return new List<Model>()
                {
                    new Model()
                    {
                        Status = ModelStatus.Success.ToString().ToLower(),
                        Title = "SITE1",
                        Type = "web",
                        Description = "description1",
                        Path = "/1/1/2"
                    },
                    new Model()
                    {
                        Status = ModelStatus.Error.ToString().ToLower(),
                        Title = "LIST1",
                        Type = "list",
                        Description = "description2",
                        Path = "/1/1/2"
                    },
                    new Model()
                    {
                        Status = ModelStatus.Warning.ToString().ToLower(),
                        Title = "LIST2",
                        Type = "list",
                        Description = "description3",
                        Path = "/1/1/2"
                    }
                    ,
                    new Model()
                    {
                        Status = ModelStatus.Warning.ToString().ToLower(),
                        Title = "LIST2",
                        Type = "list",
                        Description = "description3",
                        Path = "/1/1/2"
                    }
                    ,
                    new Model()
                    {
                        Status = ModelStatus.Warning.ToString().ToLower(),
                        Title = "LIST2",
                        Type = "list",
                        Description = "description3",
                        Path = "/1/1/2"
                    },
                     new Model()
                    {
                        Status = ModelStatus.Success.ToString().ToLower(),
                        Title = "SITE1",
                        Type = "web",
                        Description = "description1",
                        Path = "/1/1/2"
                    },
                };
            }
            else if (i == 1)
            {
                return new List<Model>()
                {
                    new Model()
                    {
                        Status = ModelStatus.Success.ToString().ToLower(),
                        Title = "SITE1",
                        Type = "web",
                        Description = "description1",
                        Path = "/1/1/2"
                    },
                    new Model()
                    {
                        Status = ModelStatus.Success.ToString().ToLower(),
                        Title = "LIST1",
                        Type = "list",
                        Description = "description2",
                        Path = "/1/1/2"
                    },
                    new Model()
                    {
                        Status = ModelStatus.Success.ToString().ToLower(),
                        Title = "LIST2",
                        Type = "list",
                        Description = "description3",
                        Path = "/1/1/2"
                    }
                    ,
                    new Model()
                    {
                        Status = ModelStatus.Warning.ToString().ToLower(),
                        Title = "LIST2",
                        Type = "list",
                        Description = "description3",
                        Path = "/1/1/2"
                    }
                    ,
                    new Model()
                    {
                        Status = ModelStatus.Warning.ToString().ToLower(),
                        Title = "LIST2",
                        Type = "list",
                        Description = "description3",
                        Path = "/1/1/2"
                    },
                     new Model()
                    {
                        Status = ModelStatus.Success.ToString().ToLower(),
                        Title = "SITE1",
                        Type = "web",
                        Description = "description1",
                        Path = "/1/1/2"
                    },
                };
            }
            else
            {
                return new List<Model>()
                {
                    new Model()
                    {
                        Status = ModelStatus.Success.ToString().ToLower(),
                        Title = "SITE1",
                        Type = "web",
                        Description = "description1",
                        Path = "/1/1/2"
                    },
                    new Model()
                    {
                        Status = ModelStatus.Success.ToString().ToLower(),
                        Title = "LIST1",
                        Type = "list",
                        Description = "description2",
                        Path = "/1/1/2"
                    },
                    new Model()
                    {
                        Status = ModelStatus.Success.ToString().ToLower(),
                        Title = "LIST2",
                        Type = "list",
                        Description = "description3",
                        Path = "/1/1/2"
                    }
                    ,
                    new Model()
                    {
                        Status = ModelStatus.Success.ToString().ToLower(),
                        Title = "LIST2",
                        Type = "list",
                        Description = "description3",
                        Path = "/1/1/2"
                    }
                    ,
                    new Model()
                    {
                        Status = ModelStatus.Success.ToString().ToLower(),
                        Title = "LIST2",
                        Type = "list",
                        Description = "description3",
                        Path = "/1/1/2"
                    },
                     new Model()
                    {
                        Status = ModelStatus.Success.ToString().ToLower(),
                        Title = "SITE1",
                        Type = "web",
                        Description = "description1",
                        Path = "/1/1/2"
                    },
                };
            }
        }

    }

    public class Model
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public string Path { get; set; }
    }

    public enum ModelStatus
    {
        Success,
        Warning,
        Error
    }

    public enum ModelType
    {
        Site,
        List
    }
}
